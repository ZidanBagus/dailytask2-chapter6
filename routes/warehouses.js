const router = require('express').Router()
// Controller
const Warehouse = require('../controller/warehouseController')

router.post('/create', Warehouse.createWarehouse)
router.get('/:id', Warehouse.findWarehouseById)


module.exports = router